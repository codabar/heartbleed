# Heartbleed Test (CVE-2014-0160) and Key Extraction
----

Based on @benmmurphy work and explanation for CloudFlare's Heartbleed challenge

## Reference
1. https://news.ycombinator.com/item?id=7576389
2. https://gist.githubusercontent.com/benmmurphy/12999c91a4d328b749e3/raw/9bcd402e3d9beec740a61a1585e24c36dea80859/heartbeat.py
3. https://github.com/jjarmoc/csaw2012_cert_app/blob/master/lib...
4. https://github.com/ius/rsatool